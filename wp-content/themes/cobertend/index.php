<?php get_header();?>
	<?php if (have_posts()) : ?>
		<div class="container-header bgParallax" id="inicio" data-speed="-5">
			<div class="guide">
				<div class="container-header__soon">
					<img src="<?php print bloginfo('template_directory');?>/img/logo-cobertend.svg" alt="Logo Cobertend">
				</div>
			</div>
		</div>
		<div class="total" id="setaDown">
			<div class="container-header__button-bottom bgParallax"></div>
		</div>
		<div class="container-protect-event bgParallax" id="clima" data-speed="111">
			<div class="guide">
				<div class="container-protect-event__text-protect">
					<img src="<?php print bloginfo('template_directory');?>/img/icone-tempo.svg" alt="ICONE PROTEGENDO SEUS EVENTOS DOS EFEITOS CLIMATICOS">
					<h2>PROTEGENDO SEUS EVENTOS DOS EFEITOS CLIMÁTICOS</h2>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="guide">
				<div class="list-images" id="galery" rel='m_PageScroll2id'>
					<h2>O QUE FAZEMOS</h2>	
					<div class="total-img">	
						<ul>
							<li>
								<a href="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-branca/lake.jpg" title="" data-src="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-branca/lake.jpg" data-thumbnail="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-branca/thumbs/thumbs_lake.jpg" data-image-id="13" data-title="lake" data-description="" class="ngg-fancybox" rel="ae2bf99b5964565362e5b99f62ef7dbe">
									<h3>TENDAS</h3>
									<h4>LONA BRANCA</h4>
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="44px" height="44px" viewBox="176.64 299.945 260.5 260.5" enable-background="new 176.64 299.945 260.5 260.5"
										 xml:space="preserve">
									<g>
										<path fill="#FFFFFF" d="M274.244,381.225c-9.015,0-16.324,7.308-16.324,16.324c0,9.015,7.309,16.322,16.324,16.322
											s16.322-7.308,16.322-16.322C290.566,388.533,283.258,381.225,274.244,381.225z M274.244,405.709c-4.508,0-8.162-3.654-8.162-8.16
											c0-4.508,3.654-8.162,8.162-8.162c4.507,0,8.162,3.654,8.162,8.162C282.406,402.055,278.75,405.709,274.244,405.709z"/>
										<path fill="#FFFFFF" d="M355.86,364.902h-97.94c-9.015,0-16.322,7.308-16.322,16.322v97.94c0,9.015,7.308,16.322,16.322,16.322
											h97.94c9.015,0,16.322-7.308,16.322-16.322v-97.94C372.183,372.21,364.875,364.902,355.86,364.902z M257.92,487.326
											c-4.508,0-8.162-3.654-8.162-8.16v-3.837l32.422-29.036l41.038,41.034H257.92z M364.022,479.165c0,4.507-3.654,8.16-8.162,8.16
											h-21.091L304.7,456.864l34.836-34.836l24.486,24.486V479.165z M364.022,434.796l-24.486-25.005l-40.571,41.262l-16.56-16.778
											l-32.648,29.938v-82.989c0-4.507,3.654-8.162,8.162-8.162h97.94c4.508,0,8.162,3.655,8.162,8.162V434.796z"/>
										<path fill="#FFFFFF" d="M306.89,309.195c-66.72,0-121,54.28-121,121s54.28,121,121,121s121-54.28,121-121
											S373.61,309.195,306.89,309.195z M306.89,542.195c-61.757,0-112-50.243-112-112s50.243-112,112-112s112,50.243,112,112
											S368.647,542.195,306.89,542.195z"/>
										</g>
									</svg>
								</a>
							</li>
							<li>
								<a href="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-acrilica/photo-1453974336165-b5c58464f1ed.jpeg" title="" data-src="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-acrilica/photo-1453974336165-b5c58464f1ed.jpeg" data-thumbnail="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-acrilica/thumbs/thumbs_photo-1453974336165-b5c58464f1ed.jpeg" data-image-id="15" data-title="photo-1453974336165-b5c58464f1ed" data-description="" class="ngg-fancybox" rel="48e6655fac4b5b68ac5f196e7dbc8e89">
									<h3>TENDAS</h3>
									<h4>LONA ACRÍLICA</h4>
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="44px" height="44px" viewBox="176.64 299.945 260.5 260.5" enable-background="new 176.64 299.945 260.5 260.5"
										 xml:space="preserve">
									<g>
										<path fill="#FFFFFF" d="M274.244,381.225c-9.015,0-16.324,7.308-16.324,16.324c0,9.015,7.309,16.322,16.324,16.322
											s16.322-7.308,16.322-16.322C290.566,388.533,283.258,381.225,274.244,381.225z M274.244,405.709c-4.508,0-8.162-3.654-8.162-8.16
											c0-4.508,3.654-8.162,8.162-8.162c4.507,0,8.162,3.654,8.162,8.162C282.406,402.055,278.75,405.709,274.244,405.709z"/>
										<path fill="#FFFFFF" d="M355.86,364.902h-97.94c-9.015,0-16.322,7.308-16.322,16.322v97.94c0,9.015,7.308,16.322,16.322,16.322
											h97.94c9.015,0,16.322-7.308,16.322-16.322v-97.94C372.183,372.21,364.875,364.902,355.86,364.902z M257.92,487.326
											c-4.508,0-8.162-3.654-8.162-8.16v-3.837l32.422-29.036l41.038,41.034H257.92z M364.022,479.165c0,4.507-3.654,8.16-8.162,8.16
											h-21.091L304.7,456.864l34.836-34.836l24.486,24.486V479.165z M364.022,434.796l-24.486-25.005l-40.571,41.262l-16.56-16.778
											l-32.648,29.938v-82.989c0-4.507,3.654-8.162,8.162-8.162h97.94c4.508,0,8.162,3.655,8.162,8.162V434.796z"/>
										<path fill="#FFFFFF" d="M306.89,309.195c-66.72,0-121,54.28-121,121s54.28,121,121,121s121-54.28,121-121
											S373.61,309.195,306.89,309.195z M306.89,542.195c-61.757,0-112-50.243-112-112s50.243-112,112-112s112,50.243,112,112
											S368.647,542.195,306.89,542.195z"/>
										</g>
									</svg>
								</a>
							</li>
							<li>
								<a href="http://localhost:8888/cobertend/wp-content/gallery/coberturas-sob-medida/norwaydock.jpg" title="" data-src="http://localhost:8888/cobertend/wp-content/gallery/coberturas-sob-medida/norwaydock.jpg" data-thumbnail="http://localhost:8888/cobertend/wp-content/gallery/coberturas-sob-medida/thumbs/thumbs_norwaydock.jpg" data-image-id="18" data-title="norwaydock" data-description="" class="ngg-fancybox" rel="4acc370ce35cf787743967b9dda68681">
									<h3>COBERTURAS</h3>
									<h4>SOB MEDIDA</h4>
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="44px" height="44px" viewBox="176.64 299.945 260.5 260.5" enable-background="new 176.64 299.945 260.5 260.5"
										 xml:space="preserve">
									<g>
										<path fill="#FFFFFF" d="M274.244,381.225c-9.015,0-16.324,7.308-16.324,16.324c0,9.015,7.309,16.322,16.324,16.322
											s16.322-7.308,16.322-16.322C290.566,388.533,283.258,381.225,274.244,381.225z M274.244,405.709c-4.508,0-8.162-3.654-8.162-8.16
											c0-4.508,3.654-8.162,8.162-8.162c4.507,0,8.162,3.654,8.162,8.162C282.406,402.055,278.75,405.709,274.244,405.709z"/>
										<path fill="#FFFFFF" d="M355.86,364.902h-97.94c-9.015,0-16.322,7.308-16.322,16.322v97.94c0,9.015,7.308,16.322,16.322,16.322
											h97.94c9.015,0,16.322-7.308,16.322-16.322v-97.94C372.183,372.21,364.875,364.902,355.86,364.902z M257.92,487.326
											c-4.508,0-8.162-3.654-8.162-8.16v-3.837l32.422-29.036l41.038,41.034H257.92z M364.022,479.165c0,4.507-3.654,8.16-8.162,8.16
											h-21.091L304.7,456.864l34.836-34.836l24.486,24.486V479.165z M364.022,434.796l-24.486-25.005l-40.571,41.262l-16.56-16.778
											l-32.648,29.938v-82.989c0-4.507,3.654-8.162,8.162-8.162h97.94c4.508,0,8.162,3.655,8.162,8.162V434.796z"/>
										<path fill="#FFFFFF" d="M306.89,309.195c-66.72,0-121,54.28-121,121s54.28,121,121,121s121-54.28,121-121
											S373.61,309.195,306.89,309.195z M306.89,542.195c-61.757,0-112-50.243-112-112s50.243-112,112-112s112,50.243,112,112
											S368.647,542.195,306.89,542.195z"/>
										</g>
									</svg>
								</a>
							</li>
							<li>
								<a href="http://localhost:8888/cobertend/wp-content/gallery/Layouts-%26-Croquis/norwaytunnel.jpg" title="" data-src="http://localhost:8888/cobertend/wp-content/gallery/Layouts-%26-Croquis/norwaytunnel.jpg" data-thumbnail="http://localhost:8888/cobertend/wp-content/gallery/Layouts-%26-Croquis/thumbs/thumbs_norwaytunnel.jpg" data-image-id="22" data-title="norwaytunnel" data-description="" class="ngg-fancybox" rel="862e4fd1c5f23712af6db4108344cdc1">
									<h4>PISO, PALCO <br> & TABLADO</h4>
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="44px" height="44px" viewBox="176.64 299.945 260.5 260.5" enable-background="new 176.64 299.945 260.5 260.5"
										 xml:space="preserve">
									<g>
										<path fill="#FFFFFF" d="M274.244,381.225c-9.015,0-16.324,7.308-16.324,16.324c0,9.015,7.309,16.322,16.324,16.322
											s16.322-7.308,16.322-16.322C290.566,388.533,283.258,381.225,274.244,381.225z M274.244,405.709c-4.508,0-8.162-3.654-8.162-8.16
											c0-4.508,3.654-8.162,8.162-8.162c4.507,0,8.162,3.654,8.162,8.162C282.406,402.055,278.75,405.709,274.244,405.709z"/>
										<path fill="#FFFFFF" d="M355.86,364.902h-97.94c-9.015,0-16.322,7.308-16.322,16.322v97.94c0,9.015,7.308,16.322,16.322,16.322
											h97.94c9.015,0,16.322-7.308,16.322-16.322v-97.94C372.183,372.21,364.875,364.902,355.86,364.902z M257.92,487.326
											c-4.508,0-8.162-3.654-8.162-8.16v-3.837l32.422-29.036l41.038,41.034H257.92z M364.022,479.165c0,4.507-3.654,8.16-8.162,8.16
											h-21.091L304.7,456.864l34.836-34.836l24.486,24.486V479.165z M364.022,434.796l-24.486-25.005l-40.571,41.262l-16.56-16.778
											l-32.648,29.938v-82.989c0-4.507,3.654-8.162,8.162-8.162h97.94c4.508,0,8.162,3.655,8.162,8.162V434.796z"/>
										<path fill="#FFFFFF" d="M306.89,309.195c-66.72,0-121,54.28-121,121s54.28,121,121,121s121-54.28,121-121
											S373.61,309.195,306.89,309.195z M306.89,542.195c-61.757,0-112-50.243-112-112s50.243-112,112-112s112,50.243,112,112
											S368.647,542.195,306.89,542.195z"/>
										</g>
									</svg>
								</a>
							</li>
							<li>
								<a href="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-branca/lake.jpg" title="" data-src="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-branca/lake.jpg" data-thumbnail="http://localhost:8888/cobertend/wp-content/gallery/tendas-lona-branca/thumbs/thumbs_lake.jpg" data-image-id="13" data-title="lake" data-description="" class="ngg-fancybox" rel="ae2bf99b5964565362e5b99f62ef7dbe">
									<h4>LAYOUT'S <br> & CROQUIS</h4>
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="44px" height="44px" viewBox="176.64 299.945 260.5 260.5" enable-background="new 176.64 299.945 260.5 260.5"
										 xml:space="preserve">
										<g>
											<path fill="#FFFFFF" d="M274.244,381.225c-9.015,0-16.324,7.308-16.324,16.324c0,9.015,7.309,16.322,16.324,16.322
												s16.322-7.308,16.322-16.322C290.566,388.533,283.258,381.225,274.244,381.225z M274.244,405.709c-4.508,0-8.162-3.654-8.162-8.16
												c0-4.508,3.654-8.162,8.162-8.162c4.507,0,8.162,3.654,8.162,8.162C282.406,402.055,278.75,405.709,274.244,405.709z"/>
											<path fill="#FFFFFF" d="M355.86,364.902h-97.94c-9.015,0-16.322,7.308-16.322,16.322v97.94c0,9.015,7.308,16.322,16.322,16.322
												h97.94c9.015,0,16.322-7.308,16.322-16.322v-97.94C372.183,372.21,364.875,364.902,355.86,364.902z M257.92,487.326
												c-4.508,0-8.162-3.654-8.162-8.16v-3.837l32.422-29.036l41.038,41.034H257.92z M364.022,479.165c0,4.507-3.654,8.16-8.162,8.16
												h-21.091L304.7,456.864l34.836-34.836l24.486,24.486V479.165z M364.022,434.796l-24.486-25.005l-40.571,41.262l-16.56-16.778
												l-32.648,29.938v-82.989c0-4.507,3.654-8.162,8.162-8.162h97.94c4.508,0,8.162,3.655,8.162,8.162V434.796z"/>
											<path fill="#FFFFFF" d="M306.89,309.195c-66.72,0-121,54.28-121,121s54.28,121,121,121s121-54.28,121-121
												S373.61,309.195,306.89,309.195z M306.89,542.195c-61.757,0-112-50.243-112-112s50.243-112,112-112s112,50.243,112,112
												S368.647,542.195,306.89,542.195z"/>
										</g>
									</svg>
								</a>
							</li>
						</ul>

						<?php
							$galeira = get_post(84);
							print do_shortcode($galeira->post_content);
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="container-location">
			<div class="guide">
				<div class="container-location__tents">
					<h2>LOCAMOS TENDAS E TABLADOS</h2>
					<h3>PARA EVENTOS EM GERAL</h3>
				</div>
			</div>
		</div>
		<div class="container-button-top" id="voltar">
			<div class="guide">
				<div class="container-button-top__buttom">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 width="55px" height="55px" viewBox="0 0 397.841 397.841" enable-background="new 0 0 397.841 397.841"
						 xml:space="preserve">
					<g>
						<path fill="#FFFFFF" d="M191.089,0c5.221,0,10.442,0,15.663,0c0.558,0.09,1.113,0.238,1.674,0.262
							c13.105,0.551,26.034,2.383,38.72,5.691c51.262,13.369,91.297,42.395,119.913,86.91c17.875,27.806,27.783,58.402,30.206,91.395
							c0.175,2.383,0.384,4.763,0.577,7.145c0,5.012,0,10.024,0,15.037c-0.072,0.247-0.19,0.49-0.209,0.741
							c-0.344,4.469-0.595,8.947-1.027,13.407c-1.107,11.427-3.345,22.646-6.614,33.644c-12.745,42.893-37.03,77.828-72.9,104.53
							c-30.71,22.861-65.306,35.622-103.513,38.504c-2.277,0.172-4.551,0.383-6.826,0.576c-5.221,0-10.442,0-15.663,0
							c-0.506-0.088-1.009-0.22-1.518-0.258c-3.119-0.23-6.244-0.399-9.36-0.663c-10.658-0.903-21.152-2.789-31.487-5.51
							c-30.553-8.045-57.675-22.549-81.367-43.447c-19.15-16.892-34.501-36.744-46.024-59.527C9.37,264.782,2.406,239.729,0.56,213.27
							c-0.159-2.279-0.372-4.554-0.56-6.831c0-5.012,0-10.024,0-15.037c0.086-0.507,0.215-1.011,0.252-1.521
							c0.227-3.12,0.397-6.244,0.654-9.36c0.986-11.967,3.109-23.745,6.481-35.258c15.203-51.912,46.182-91.628,92.836-118.929
							c25.925-15.171,54.063-23.579,84.041-25.758C186.54,0.411,188.814,0.193,191.089,0z M382.52,198.963
							c0.085-101.237-82.161-183.515-183.554-183.636C98.116,15.207,16.144,97.103,15.308,197.311
							c-0.853,102.143,82.137,185.169,183.416,185.222C300.047,382.585,382.435,300.382,382.52,198.963z"/>
						<path fill="#FFFFFF" d="M198.777,229.399c-12.474,0-24.953-0.226-37.419,0.09c-6.812,0.173-10.513-6.458-7.159-12.354
							c12.718-22.362,25.245-44.833,37.817-67.279c1.519-2.712,3.634-4.463,6.813-4.466c3.173-0.003,5.302,1.713,6.826,4.435
							c12.645,22.583,25.33,45.144,37.985,67.721c2.631,4.693,0.655,10.129-4.192,11.549c-0.929,0.272-1.955,0.291-2.936,0.292
							C223.933,229.405,211.355,229.399,198.777,229.399z M173.279,213.943c17.17,0,34.033,0,51.115,0
							c-8.545-15.237-16.988-30.291-25.55-45.557C190.274,183.656,181.833,198.699,173.279,213.943z"/>
					</g>
					</svg>
				</div>
			</div>
		</div>
	<?php endif;?>
		<?php get_footer();?>
	</body>
</html>