<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<meta author="Hedellen Fernandes">
		<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
		<link href='http://fonts.googleapis.com/css?family=Raleway:500,400,100' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory');?>/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">
		<script type="text/javascript" src="<?php bloginfo('template_directory');?>/jquery-1.11.2.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory');?>/functions.js"></script>
	</head>
	<body>
		<div class="geral-menu">
			<div class="container-header__icon-menu"></div>
		</div>
		<div class="geral-menu-clicado">
			<div class="clicado"></div>
		</div>
		<header>
			<div class="guide">
				<menu id="menu">
					<?php
					wp_nav_menu(); ?>
				</menu>
			</div>
		</header>
