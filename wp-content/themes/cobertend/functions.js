$(document).ready(function(){

	// ALTURA TELA PRINCIPAL 
	var altura = $(window).height();
	$('.container-header').css('height',altura)
	$('.container-header').resize(function(){
		$(this).css('height',altura)
	});
	
	//SCROLL TO ID 


	$( '#menu-item-5' ).click(function() {

		$('html,body').animate({
			scrollTop: $('#inicio').offset().top
		},700)
	});

	$( '.container-button-top__buttom' ).click(function() {

		$('html,body').animate({
			scrollTop: $('#inicio').offset().top
		},700)
	});

	$( '#menu-item-6' ).click(function() {

		$('html,body').animate({
			scrollTop: $('#galery').offset().top
		},700)
	});

	$( '#setaDown' ).click(function() {

		$('html,body').animate({
			scrollTop: $('#clima').offset().top
		},700)
	});

	$( '#menu-item-7' ).click(function() {

		$('html,body').animate({
			scrollTop: $('#contact').offset().top
		},700)
	});

	//ANIMATE BUTTOM MENU

	$('.container-header__icon-menu').animate({marginTop:'0px'})
	$('.clicado').animate({marginTop:'0px'})

	$('.geral-menu').click(function(){
		$(this).css('display','none')
		$('.geral-menu-clicado').css('display','block')
		$('.clicado').css('display','block')
		$('.menu').fadeIn(100);
	});

	$('.geral-menu-clicado').click(function(){
		$(this).css('display','none')
		$('.geral-menu').css('display','block')
		$('.menu').fadeOut(100);
	});

	//PARALLAX
	 
	$('div.bgParallax').each(function(){
		var $obj = $(this);
	 
		$(window).scroll(function() {
			var yPos = -($(window).scrollTop() / $obj.data('speed')); 
	 
			var bgpos = '50% '+ yPos + 'px';
	 
			$obj.css('background-position', bgpos );
	 
		}); 
	});

	window.onscroll = scroll;

	function scroll(){

		var y     = window.pageYOffset;
		var width = $('body').width();

		if(y >= 0 && y <= 1500){$('#voltar').fadeOut();}
		else {$('#voltar').fadeIn();}

	}

});