<?php
	
	// ATIVANDO O BOTAO DE MENUS
	register_nav_menus( array(
		'primary'   => __( 'Top primary menu', 'twentyfourteen' ),
		'secondary' => __( 'Secondary menu in left sidebar', 'twentyfourteen' ),
	) 
	);
	/***
	    Adiciona logo personalizado 
	***/
	function my_login_logo() {?>
	<style type="text/css">
	   body.login div#login h1 a {
	        background-image: url('wp-content/themes/coberlend/img/logo_login.png' ) !important;
	        width:312px;
	        height:94px;
	        background-size: auto auto;
	       }
	   </style>
	<?php }


	add_action( 'login_enqueue_scripts', 'my_login_logo' );

	/***
	    Remove o logo de admin do painel do wordpress
	***/
	function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node('wp-logo');
	}

	add_action('admin_bar_menu', 'remove_wp_logo', 999);

	/***
	Hack para title da Home
	***/
	add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
	function baw_hack_wp_title_for_home( $title )
	{
	  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
	    return __( 'Início', 'theme_domain' ) . ' | ' . get_bloginfo( 'description' );
	  }
	  return $title. ' | ' . get_bloginfo( 'description' );
	}
?>	

	