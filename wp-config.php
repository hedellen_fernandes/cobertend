<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'cobertend');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '},mlEG3NSVZ5~,_ kYW`SE8UwCq8yQ6$EBoul/S4$NQ2cnx3@C+GH#-orFM$-lu&');
define('SECURE_AUTH_KEY',  ']h!h2q4R1$-Bllw)26wE)$_ps#h)~( ZF[`)A$0q`^R9Sp?SZjtoP(fO-L=~Zw$*');
define('LOGGED_IN_KEY',    '1QC Hn~p1*1k&{Z0GNiHP!+IyT_X!sbyx)!q>%@`c@5<q*Q~~|,=?{g+wW<G%#<}');
define('NONCE_KEY',        'A%}22>LsM*qIEK3[7V[g*u^=me+E6%[zV;y9yA)b*+5SX5=.Tr~M7w|yky#blb7B');
define('AUTH_SALT',        'HZ*Gr,gqZ(}y>%s5As%^9FKCyhv:{rwe<w[VI^$Np/@VL&#WQSB&9I-uCZ>&4mMz');
define('SECURE_AUTH_SALT', '/j{-< ~ieG]sFA@dlBn|cP*#La]2*19zp/2C;~fUncL ,dZx=Mc9C>W):-Nqe>pf');
define('LOGGED_IN_SALT',   'a0Ry{q,}5ee>A`5Zgh*To6h^_V.{XaM?yZLNF=#h]n#S>]6}lQ>BEGG;8ltIZ|x^');
define('NONCE_SALT',       'pnzA3tk?O&Z.=ERTUPc=c==_R#2#LZH~%J/c*0hbu5A>d]sMn^g4JE8Mc4(G(5tO');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
